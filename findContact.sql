﻿-- Function: public.find_contact_by_id(integer)

-- DROP FUNCTION public.find_contact_by_id(integer);

CREATE OR REPLACE FUNCTION public.find_contact_by_id(_user_id integer)
  RETURNS SETOF _contact AS
$BODY$begin
return query select * from _contact as t where user_id=_user_id;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.find_contact_by_id(integer)
  OWNER TO postgres;
