package carExample.service;

import carExample.bean.Car;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class Runner {

    public String run() {
        ApplicationContext context = new AnnotationConfigApplicationContext(InitCar.class);
        Car car = (Car) context.getBean("getCar");
        return car.getName();
    }
}
