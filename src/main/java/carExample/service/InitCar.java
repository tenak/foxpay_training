package carExample.service;

import carExample.bean.Car;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitCar {
    @Bean
    public Car getCar() {
        return new Car("ABC", "1");
    }
}
