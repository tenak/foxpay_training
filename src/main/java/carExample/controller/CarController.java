package carExample.controller;

import carExample.service.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarController {
    @Autowired
    Runner runner;

    @RequestMapping(value = "getCar")
    public String getCar() {
        return runner.run();
    }
}
