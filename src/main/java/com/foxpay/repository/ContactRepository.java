package com.foxpay.repository;


import com.foxpay.bean.database.ContactDB;
import org.springframework.data.repository.CrudRepository;


public interface ContactRepository extends CrudRepository<ContactDB, Integer> {

}