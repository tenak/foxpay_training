package com.foxpay.repository;

import com.foxpay.bean.database.UserTokenDB;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TokenRepository extends CrudRepository<UserTokenDB, Integer> {
    Optional<UserTokenDB> findByUserToken(String token);
}
