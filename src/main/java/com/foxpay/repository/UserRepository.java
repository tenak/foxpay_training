package com.foxpay.repository;

import com.foxpay.bean.database.UserDB;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserDB, Integer> {
    Optional<UserDB> findByUsername(String username);
}
