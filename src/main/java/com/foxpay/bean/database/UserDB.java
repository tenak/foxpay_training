package com.foxpay.bean.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "_user")
public class UserDB {
    @Id
    @Column(name = "user_id")
    private Integer userID;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;

    public UserDB() {

    }

    public UserDB(Integer userID, String username, String password) {
        this.userID = userID;
        this.username = username;
        this.password = password;

    }

    public Integer getUserID() {
        return userID;
    }


    public String getUsername() {
        return username;
    }


    public String getPassword() {
        return password;
    }

}
