package com.foxpay.bean.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "_user_token")
public class UserTokenDB {
    @Id
    @Column(name = "user_id")
    private Integer userID;
    @Column(name = "user_token")
    private String userToken;

    public UserTokenDB() {

    }

    public UserTokenDB(Integer userID, String userToken) {
        this.userID = userID;
        this.userToken = userToken;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
