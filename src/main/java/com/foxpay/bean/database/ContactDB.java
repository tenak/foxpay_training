package com.foxpay.bean.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "_contact")
public class ContactDB {
    @Id
    @Column(name = "user_id")
    private Integer userID;
    @Column(name = "contact_json")
    private String contactJson;

    public ContactDB() {

    }

    public ContactDB(Integer userID, String contactJson) {
        this.userID = userID;
        this.contactJson = contactJson;
    }

    public Integer getUserID() {
        return userID;
    }

    public String getContactJson() {
        return contactJson;
    }

    public void setContactJson(String contactJson) {
        this.contactJson = contactJson;
    }

}
