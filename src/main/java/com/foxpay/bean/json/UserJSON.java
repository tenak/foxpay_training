package com.foxpay.bean.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Id;
import java.util.ArrayList;

public class UserJSON {
    @Id
    @JsonProperty("userID")
    private Integer userID;
    @JsonProperty("contact_json")
    private ArrayList<ContactJSON> listContact;

    public UserJSON() {
    }

    public UserJSON(Integer userID, ArrayList<ContactJSON> listContact) {
        this.userID = userID;
        this.listContact = listContact;
    }

    public Integer getUserID() {
        return userID;
    }

    public ArrayList<ContactJSON> getListContact() {
        return listContact;
    }
}
