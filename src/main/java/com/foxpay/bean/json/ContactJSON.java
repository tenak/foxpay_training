package com.foxpay.bean.json;

public class ContactJSON {

    private String name;
    private String phoneNumber;
    private String type;

    public ContactJSON() {

    }

    public ContactJSON(String name, String phoneNumber, String type) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getType() {
        return type;
    }
}
