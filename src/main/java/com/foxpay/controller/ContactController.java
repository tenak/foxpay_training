package com.foxpay.controller;

import com.foxpay.bean.json.UserJSON;
import com.foxpay.service.ContactService;
import com.foxpay.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactController {
    @Autowired
    ContactService contactService;
    @Autowired
    UserService userService;

    @PostMapping(value = "saveContact")
    public UserJSON insertContact(@RequestBody UserJSON user) {
        contactService.saveContact(user);
        return user;
    }

    @PostMapping(value = "deleteContact")
    public boolean deleteContact(@RequestBody UserJSON user) {
        boolean result = contactService.deleteContact(user);
        return result;
    }

    @PostMapping(value = "syncContact")
    public boolean syncContact(@RequestHeader("token") String token, @RequestBody UserJSON user) {
        if (!userService.validate(token))
            return false;
        contactService.saveContact(user);
        return true;
    }

    @PostMapping(value = "takeContact")
    public UserJSON takeContact(@RequestHeader("token") String token) {
        if (!userService.validate(token))
            return null;
        UserJSON userJSON = contactService.findContact(token);
        return userJSON;
    }
}