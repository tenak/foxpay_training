package com.foxpay.controller;

import com.foxpay.bean.database.UserDB;
import com.foxpay.bean.database.UserTokenDB;
import com.foxpay.bean.json.UserJSON;
import com.foxpay.service.ContactService;
import com.foxpay.service.TokenJWTService;
import com.foxpay.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    TokenJWTService jwtService;
    @Autowired
    UserService hashService;
    @Autowired
    ContactService contactService;


    @PostMapping(value = "createTokenJWT")
    public String createTokenJWT(@RequestBody UserDB user) {
        String token = jwtService.createTokenJWT(user);
        return token;
    }

    @PostMapping(value = "getUserJWT")
    public UserDB getUserJWT(@RequestBody String token) {
        UserDB user = jwtService.getUserFromTokenJWT(token);
        return user;
    }

    @PostMapping(value = "login")
    public UserTokenDB createTokenHash(@RequestBody UserDB user) {
        UserTokenDB userToken = hashService.login(user);
        return userToken;
    }

    @PostMapping(value = "validate")
    public boolean validateTokenHash(String token) {
        boolean result = hashService.validate(token);
        return result;
    }

    @PostMapping(value = "logout")
    public boolean invalidateTokenHash(String token) {
        boolean result = hashService.logout(token);
        return result;
    }

    @PostMapping(value = "getUserDB")
    public UserJSON getUserFromDB(Integer userID) {
        return contactService.getContactDB(userID);
    }
}