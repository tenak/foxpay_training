package com.foxpay.service;

import com.foxpay.bean.database.ContactDB;
import com.foxpay.bean.database.UserTokenDB;
import com.foxpay.bean.json.ContactJSON;
import com.foxpay.bean.json.UserJSON;
import com.foxpay.repository.ContactRepository;
import com.foxpay.repository.TokenRepository;
import com.foxpay.tool.ConvertContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class ContactService {
    @Autowired
    ContactRepository contactRepository;
    @Autowired
    TokenRepository tokenRepository;
    @Autowired
    ConvertContact convertContact;
    @PersistenceContext
    EntityManager entityManager;

    public void saveContact(UserJSON user) {
        String contactJson = convertContact.getStringContactJSON(user.getListContact());
        ContactDB contactDB = new ContactDB(user.getUserID(), contactJson);
        contactRepository.save(contactDB);
    }

    public UserJSON findContact(String token) {
        Optional<UserTokenDB> optionalUserTokenDB = tokenRepository.findByUserToken(token);
        if (!optionalUserTokenDB.isPresent())
            return null;
        UserTokenDB userTokenDB = optionalUserTokenDB.get();
        ContactDB contactDB = contactRepository.findById(userTokenDB.getUserID()).get();
        return new UserJSON(userTokenDB.getUserID(), convertContact.getListContact(contactDB.getContactJson()));
    }

    public boolean deleteContact(UserJSON user) {
        Optional<ContactDB> object = contactRepository.findById(user.getUserID());
        if (object.isPresent()) {
            ContactDB contactDB = object.get();
            contactRepository.delete(contactDB);
            return true;
        }
        return false;
    }

    public UserJSON getContactDB(Integer userID) {
        StoredProcedureQuery findContactById = entityManager.createStoredProcedureQuery("find_contact_by_id");
        findContactById.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);

        findContactById.setParameter(1, userID);
        List<Object> result = (List<Object>) findContactById.getResultList();
        Iterator itr = result.iterator();
        UserJSON userJSON = null;
        if (itr.hasNext()) {
            Object[] obj = (Object[]) itr.next();
//            int id = (Integer.parseInt(String.valueOf(obj[0])));
            String contact_json = String.valueOf(obj[1]);
            ArrayList<ContactJSON> listContact = convertContact.getListContact(contact_json);
            userJSON = new UserJSON(userID, listContact);
        }

        return userJSON;
    }
}
