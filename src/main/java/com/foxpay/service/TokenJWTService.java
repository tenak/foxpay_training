package com.foxpay.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.foxpay.bean.database.UserDB;
import com.foxpay.tool.HashFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenJWTService {
    private static final String TOKEN_SECRET = "s4T2zOIWHMM1sxq";
    @Autowired
    HashFunction hashFunction;

    public String createTokenJWT(UserDB user) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            String token = JWT.create().withClaim("userID", user.getUserID())
                    .withClaim("username", user.getUsername())
                    .withClaim("password", user.getPassword())
                    .withClaim("random", hashFunction.generateRamdom())
                    .sign(algorithm);
            return token;
        } catch (JWTCreationException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public UserDB getUserFromTokenJWT(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            Integer userID = jwt.getClaim("userID").asInt();
            String username = jwt.getClaim("username").asString();
            String password = jwt.getClaim("password").asString();
            return new UserDB(userID, username, password);
        } catch (JWTVerificationException exception) {
            exception.printStackTrace();
            return null;
        }
    }
}