package com.foxpay.service;

import com.foxpay.bean.database.UserDB;
import com.foxpay.bean.database.UserTokenDB;
import com.foxpay.repository.TokenRepository;
import com.foxpay.repository.UserRepository;
import com.foxpay.tool.HashFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    TokenRepository tokenRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    HashFunction hashFunction;

    public UserTokenDB login(UserDB user) {
        String message = user.getUsername() + "," + hashFunction.getHash(user.getPassword()) + "," + hashFunction.generateRamdom();
        String userToken = hashFunction.getHash(message);
        Optional<UserDB> optionalUserDB = userRepository.findByUsername(user.getUsername());
        UserTokenDB userTokenDB = new UserTokenDB(user.getUserID(), userToken);
        if (!optionalUserDB.isPresent()) {
            userRepository.save(new UserDB(user.getUserID(), user.getUsername(), hashFunction.getHash(user.getPassword())));
        }
        tokenRepository.save(userTokenDB);
        return userTokenDB;
    }

    public boolean validate(String token) {
        Optional<UserTokenDB> optionalUserTokenDB = tokenRepository.findByUserToken(token);
        if (!optionalUserTokenDB.isPresent())
            return false;
        return token.equals(optionalUserTokenDB.get().getUserToken());

    }

    public boolean logout(String token) {
        Optional<UserTokenDB> optionalUserTokenDB = tokenRepository.findByUserToken(token);
        if (!optionalUserTokenDB.isPresent())
            return false;
        UserTokenDB userTokenDB = optionalUserTokenDB.get();
        userTokenDB.setUserToken(null);
        tokenRepository.save(userTokenDB);
        return true;
    }
}
