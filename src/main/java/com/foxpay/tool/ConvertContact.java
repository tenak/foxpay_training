package com.foxpay.tool;

import com.foxpay.bean.json.ContactJSON;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ConvertContact {
    public ArrayList<ContactJSON> getListContact(String contactJson) {
        ArrayList<ContactJSON> list = new ArrayList<>();
        String[] listInfo = contactJson.split(";");
        for (String info : listInfo) {
            list.add(initContact(info));
        }
        return list;
    }

    private ContactJSON initContact(String contact) {
        String[] contactInfo = contact.substring(1, contact.length() - 1).split(",");
        String name = convertInfo(contactInfo[0], "name");
        String phoneNumber = convertInfo(contactInfo[1], "phoneNumber");
        String type = convertInfo(contactInfo[2], "type");
        return new ContactJSON(name, phoneNumber, type);
    }

    private String convertInfo(String info, String param) {
        return info.replace("\"", "").replace(param + ":", "");
    }

    public String getStringContactJSON(ArrayList<ContactJSON> listContact) {
        StringBuilder stringJSON = new StringBuilder();
        for (ContactJSON contactJSON : listContact) {
            stringJSON.append("{\"name\":\"" + contactJSON.getName() + "\",\"phoneNumber\":\"" + contactJSON.getPhoneNumber() +
                    "\",\"type\":\"" + contactJSON.getType() + "\"}");
        }
        return stringJSON.toString().replace("}{", "};{");
    }
}
