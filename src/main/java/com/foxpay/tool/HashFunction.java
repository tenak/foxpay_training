package com.foxpay.tool;

import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

@Service
public class HashFunction {
    public String getHash(String message) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            byte[] hash = digest.digest(message.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte i : hash) {
                sb.append(Integer.toHexString((0xff & i) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public String generateRamdom() {
        byte[] array = new byte[20];
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        return generatedString;
    }
}
