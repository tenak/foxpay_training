package com.foxpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoxpayApplication {
    public static void main(String[] args) {
        SpringApplication.run(FoxpayApplication.class, args);
    }

}

